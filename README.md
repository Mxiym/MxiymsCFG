<p align="center"><img src="https://github.com/Mxiym/IcosProjects/blob/master/logoinfernoupdate1.gif?raw=true"></p>

----------------------------------------------------------------------------------------------------------------------------------------

### EN
*Content Based on Team Fortress 2, elaborated by @Mxiym for the best functioning of the game for those who suffer with lag or the famous "lag spike"*

### PT(PT-BR)
*Conteúdo Baseado em Team Fortress 2, elaborado por @Mxiym para o melhor funcionamento do jogo para quem sofre com lag ou o famoso "lag spike"*

----------------------------------------------------------------------------------------------------------------------------------------

### English
<details>
  
MxiymsCFG is a some kind of "increasefps" for make better your team fortress 2.

### How to use MxiymsCFG?
1. *List of compatibles systems (Detailed: [**Compatibles OS**](https://github.com/Mxiym/MxiymsCFG/wiki/Sistemas-Compatíveis))*
2. *Copy MxiymsCFG to your cfg folder and paste (Detailed: [**Putting cfg in folder**](Soon))*
3. *Put it in Launch options: [**Setting up in The Launch Options**](Soon))*

### Getting in touch with other developers
- *[Discord server](https://discord.gg/xsFP8vt)*

</details>

### Português
<details>
  
MxiCFG é um tipo de "aumento de fps" para melhorar o seu Team Fortress 2.

### Como usar o MxiymsCFG?
 1. *Lista de Sistemas Operacionais(OS) Compatíveis (Detalhes: [**OS Compatíveis**](https://github.com/Mxiym/MxiymsCFG/wiki/Sistemas-Compatíveis))*
 2. *Copie MxiymsCFG para sua pasta cfg e cole (Mais: [**Colocando a .cfg na pasta**](Em Breve))*
 3. *Crie seu primeiro plugin! (Leia mais: [**Configurando nas opções de inicialização**](Em Breve))*

### Entre em contato com outros desenvolvedores
- *[Discord server](https://discord.gg/xsFP8vt)*

</details>

----------------------------------------------------------------------------------------------------------------------------------------

| Contributors  Colaboradores(Contribuições):          | Profile[Perfil]                                                               |
| ---------------------------------------------------- | ----------------------------------------------------------------------------- | 
|  - [Mxiym](https://github.com/Mxiym) [Developer]     | *[Steam](https://steamcommunity.com/id/Mxiyms)*                               |
| - Hosted_Playerss [Beta_Tester]                      | *[Steam](https://steamcommunity.com/profiles/76561198156197082/)*             |
| - available slot n:3°                                | *[Steam]()*                                                                   |
| - available slot n:4°                                | *[Steam]()*                                                                   |
| - available slot n:5°                                | *[Steam]()*                                                                   |
| - available slot n:6°                                | *[Steam]()*                                                                   |
 
---------------------------------------------------------------------------------------------------------------------------------------- 
<p align="center"><img src="https://github.com/Mxiym/IconsProjects/blob/master/MxiymsCFG/uncle-sam.gif?raw=true"></p>
